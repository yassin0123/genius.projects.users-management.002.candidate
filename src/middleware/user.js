const _ = require("lodash");
const createError = require("http-errors");
const { ObjectId } = require("mongodb");
const dbProvider = require("../db");
const { AUTH_SECRET } = require("../config");
const jwt = require("jsonwebtoken");

const COLLECTION = "User";
const ROLES = {
  STUDENT: "STUDENT",
  TEACHER: "TEACHER",
  ADMIN: "ADMIN",
};

exports.check = async (req, res, next) => {
  var token = req.headers["token"];
  if (!token) {
    return next();
  }

  jwt.verify(token, AUTH_SECRET, async function (err, decoded) {
    if (err) {
      return next();
    }

    const { _id } = decoded;
    const db = await dbProvider.get();
    const exists = await db
      .collection(COLLECTION)
      .findOne(
        { _id: ObjectId(_id) },
        { projection: { _id: 1, role: 1, accountType: 1, pack: 1 } }
      );

    if (!exists) {
      return next();
    }

    req.auth = {
      _id,
      role: exists.role,
      token,
    };

    return next();
  });
};

exports.isAuthenticated = async (req, res, next) => {
  if (!req.auth) {
    throw new createError.Unauthorized("Unauthorized operation");
  }
  return next();
};

exports.isOfRole = (role) => (req, res, next) => {
  if (!req.auth) {
    throw new createError.Unauthorized("Unauthorized operation");
  }

  if (_.get(req, "auth.role", null) !== role) {
    throw new createError.Unauthorized("Unauthorized operation");
  }

  return next();
};

exports.isOfRoles = (roles) => (req, res, next) => {
  if (!req.auth) {
    throw new createError.Unauthorized("Unauthorized operation");
  }

  if (!_.includes(roles, _.get(req.auth, "role"))) {
    throw new createError.Unauthorized("Unauthorized operation");
  }

  return next();
};

exports.isStudent = this.isOfRole(ROLES.STUDENT);
exports.isTeacher = this.isOfRole(ROLES.TEACHER);
exports.isAdmin = this.isOfRole(ROLES.ADMIN);
exports.isTeacherOrAdmin = this.isOfRoles([ROLES.TEACHER, ROLES.ADMIN]);
