const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../db");

const COLLECTION = {
  CLASSROOM: "Classroom",
  User: "User",
};

const Joi = require("joi");

const schema = Joi.object({
  name: Joi.string().required(),
});

const userExists = async (_id) => {
  if (!ObjectId.isValid(_id)) {
    return false;
  }
  const db = await dbProvider.get();
  return (
    (await db
      .collection(COLLECTION.CLASSROOM)
      .countDocuments({ _id: ObjectId(_id) })) > 0
  );
};

const validatePayload = (payload) => {
  const { error } = schema.validate(payload);

  return error;
};

exports.exists = async (req, res, next) => {
  if (!(await userExists(req.params._id))) {
    return res.sendStatus(400);
  }

  return next();
};

exports.post = (req, res, next) => {
  const error = validatePayload(req.body);
  if (error) {
    return res.sendStatus(400);
  }

  return next();
};

exports.put = async (req, res, next) => {
  if (!(await userExists(req.params._id))) {
    return res.sendStatus(400);
  }

  const error = validatePayload(req.body);
  if (error) {
    return res.sendStatus(400);
  }

  return next();
};

const isPartOfClassroom = async (_id, classroomId) => {
  if (!ObjectId.isValid(_id) || !ObjectId.isValid(req.params._id)) {
    return false;
  }

  const db = await dbProvider.get();
  return (
    (await db
      .collection(COLLECTION.User)
      .countDocuments({
        _id: ObjectId(_id),
        classroom: ObjectId(classroomId),
      })) > 0
  );
};

exports.canAccess = async (req, res, next) => {
  req.classroomId = req.params._id;
  if (req.auth.role === "ADMIN") {
    return next();
  }

  if (_.includes(["TEACHER", "STUDENT"], req.auth.role)) {
    if (!(await isPartOfClassroom(req.auth._id, req.params._id))) {
      return res.sendStatus(400);
    } else return next();
  }

  return res.sendStatus(401);
};
