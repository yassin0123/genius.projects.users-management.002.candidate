const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../db");
const COLLECTION = "Classroom";

exports.getClassrooms = async (conditions = {}) => {
  const db = await dbProvider.get();
  const classrooms = await db.collection(COLLECTION).find(conditions).toArray();
  return classrooms;
};

exports.getClassroom = async (conditions = {}) => {
  const db = await dbProvider.get();
  const classroom = await db.collection(COLLECTION).findOne(conditions);
  return classroom;
};

exports.addClassroom = async (data) => {
  const classroom = _.cloneDeep(data);
  const db = await dbProvider.get();
  await db.collection(COLLECTION).insertOne(classroom);
  return classroom;
};

exports.updateClassroom = async (data) => {
  const db = await dbProvider.get();
  const { value: classroom } = await db
    .collection(COLLECTION)
    .findOneAndUpdate(
      { _id: ObjectId(data._id) },
      { $set: { ..._.omit(data, "_id") } },
      { returnOriginal: false }
    );
  return classroom;
};

exports.deleteClassroom = async (conditions = {}) => {
  const db = await dbProvider.get();
  await db.collection(COLLECTION).deleteOne(conditions);
};
