const _ = require("lodash");
const { ObjectId } = require("mongodb");
const {
  getClassrooms,
  getClassroom,
  addClassroom,
  updateClassroom,
  deleteClassroom,
} = require("./service");

const { deleteMany: deleteManyUsers } = require("./users/service");

exports.list = async (req, res, next) => {
  const Classrooms = await getClassrooms();
  return res.json(Classrooms);
};

exports.item = async (req, res) => {
  const Classroom = await getClassroom({ _id: ObjectId(req.params._id) });
  return res.json(Classroom);
};

exports.add = async (req, res) => {
  const Classroom = await addClassroom(req.body);
  return res.status(201).send(Classroom);
};

exports.update = async (req, res) => {
  const Classroom = await updateClassroom({ _id: req.params._id, ...req.body });
  return res.status(200).send(Classroom);
};

exports.delete = async (req, res) => {
  await deleteClassroom({ _id: ObjectId(req.params._id) });
  await deleteManyUsers({ classroom: ObjectId(req.params._id) });
  return res.sendStatus(204);
};

exports.propagateParams = (req, res, next) => {
  req.classroomId = req.params._id;
  return next();
};
