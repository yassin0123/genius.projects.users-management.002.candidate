const _ = require("lodash");
const { ObjectId } = require("mongodb");
const {
  getUsers,
  getUser,
  addUser,
  updateUser,
  deleteUser,
} = require("./service");

exports.list = async (req, res, next) => {
  const users = await getUsers({ classroom: ObjectId(req.classroomId) });
  return res.json(users);
};

exports.item = async (req, res) => {
  const user = await getUser({
    _id: ObjectId(req.params._id),
    classroom: ObjectId(req.classroomId),
  });
  return res.json(user);
};

exports.add = async (req, res) => {
  const user = await addUser(req.body);
  return res.status(201).send(user);
};

exports.update = async (req, res) => {
  const user = await updateUser({ _id: req.params._id, ...req.body });
  return res.status(200).send(user);
};

exports.delete = async (req, res) => {
  await deleteUser({ _id: ObjectId(req.params._id) });
  return res.sendStatus(204);
};
