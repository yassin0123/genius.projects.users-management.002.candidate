const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../../db");
const COLLECTION = "User";

exports.getUsers = async (conditions = {}) => {
  const db = await dbProvider.get();
  const users = await db.collection(COLLECTION).find(conditions).toArray();
  return users;
};

exports.getUser = async (conditions = {}) => {
  const db = await dbProvider.get();
  const user = await db.collection(COLLECTION).findOne(conditions);
  return user;
};

exports.addUser = async (data) => {
  const user = _.cloneDeep(data);
  const db = await dbProvider.get();
  await db.collection(COLLECTION).insertOne(user);
  return user;
};

exports.updateUser = async (data) => {
  const db = await dbProvider.get();
  const { value: user } = await db
    .collection(COLLECTION)
    .findOneAndUpdate(
      { _id: ObjectId(data._id) },
      { $set: { ..._.omit(data, "_id") } },
      { returnOriginal: false }
    );
  return user;
};

exports.deleteUser = async (conditions = {}) => {
  const db = await dbProvider.get();
  await db.collection(COLLECTION).deleteOne(conditions);
};

exports.deleteMany = async (conditions = {}) => {
  const db = await dbProvider.get();
  await db.collection(COLLECTION).deleteMany(conditions);
};

