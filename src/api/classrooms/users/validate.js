const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../../db");
const COLLECTION = "User";

const Joi = require("joi");

const schema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  username: Joi.string().required(),
  email: Joi.string().required(),
  age: Joi.number().strict().positive().required(),
  role: Joi.string().valid("STUDENT", "TEACHER").required(),
});

const userExists = async (_id) => {
  if (!ObjectId.isValid(_id)) {
    return false;
  }
  const db = await dbProvider.get();
  return (
    (await db.collection(COLLECTION).countDocuments({ _id: ObjectId(_id) })) > 0
  );
};

const duplicateUsernameOrEmail = async ({ username, email }, _id) => {
  const conditions = _id
    ? { $or: [{ username }, { email }], _id: { $ne: ObjectId(_id) } }
    : { $or: [{ username }, { email }] };

  const db = await dbProvider.get();
  return (await db.collection(COLLECTION).countDocuments(conditions)) > 0;
};

const validatePayload = (payload) => {
  const { error } = schema.validate(payload);

  return error;
};

exports.exists = async (req, res, next) => {
  if (!(await userExists(req.params._id))) {
    return res.sendStatus(400);
  }

  return next();
};

exports.post = async (req, res, next) => {
  let error;
  error = validatePayload(req.body);
  if (error) {
    return res.sendStatus(400);
  }

  error = await duplicateUsernameOrEmail(req.body);

  if (error) {
    return res.sendStatus(400);
  }

  return next();
};

exports.put = async (req, res, next) => {
  let error;
  if (!(await userExists(req.params._id))) {
    return res.sendStatus(400);
  }

  error = validatePayload(req.body);
  if (error) {
    return res.sendStatus(400);
  }

  error = await duplicateUsernameOrEmail(req.body, req.params._id);

  if (error) {
    return res.sendStatus(400);
  }

  return next();
};
